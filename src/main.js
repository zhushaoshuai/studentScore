// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
//插件载入开始
import axios from './http'
import store from './store/store'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
Vue.use(iView)
//插件载入结束

Vue.config.productionTip = false
Vue.prototype.axios = axios

import '../static/reset/reset.css'

/* eslint-disable no-new */

new Vue({
  el: '#app',
  axios,
  store,
  router,
  template: '<App/>',
  components: { App }
})
