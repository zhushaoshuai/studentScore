import Vue from 'vue'
import Router from 'vue-router'
import * as types from '../store/types'
import ZssSetup from '@/zsspage/zssSetup'
import Guanli from '@/zsspage/guanli'
import Students from '@/zsspage/studentsdisplay'
import Automatic from '@/zsspage/automatic'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Automatic
    },
    {
      path: '/sdkf',
      component: ZssSetup
    },
    {
      path: '/jfgl',
      component: Guanli
    },
    {
      path: '/jfzs',
      component: Students
    }
  ]
})
